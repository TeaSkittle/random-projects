#!/bin/bash
# Author: Travis Dowd
# Date: 10/30/18(v0.1), 3/22/19(v0.2)
# Setup wifi for Void Linux/wpa_supplicant

echo "#######################"
echo "This is a simple script"
echo "to set up wifi with"
echo "wpa_supplicant."
echo "Run as superuser!"
echo "-> sudo wifi_setup.sh"
echo "#######################"
printf "\n"

echo "== Available Devices =="
ip link show
printf "\n"
echo "Enter wifi interface: "
read wifi
nmcli dev wifi
echo "Enter SSID of network: "
read network
echo "Enter passphrase for network: "
read pass
printf "\n"

echo "== Enabling Device =="
ip link set up $wifi
printf "\n"

echo "== Setting up WiFi =="
if [ ! -f /etc/wpa_supplicant/wpa_supplicant-$wifi.conf ]
    then
    cp /etc/wpa_supplicant/wpa_supplicant.conf \
    /etc/wpa_supplicant/wpa_supplicant-$wifi.conf
    wpa_passphrase $network $pass >> \
    /etc/wpa_supplicant/wpa_supplicant-$wifi.conf
else
     wpa_passphrase $network $pass >> \
    /etc/wpa_supplicant/wpa_supplicant-$wifi.conf
fi
printf "\n"

echo "######################"
echo "Setup complete"
echo "Reboot system? [Y/n]"
echo "####################"
read answer

case $answer in
  [yY])
    echo "-- Rebooting system --"
    reboot 
    ;;
  [nN])
    echo "Canceling reboot..."
    echo "Setup of wifi not complete until reboot"
    exit 1
    ;;
  esac
