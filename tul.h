// Header file for shell commands
// Author: Travis Dowd
// Date: 9-20-19
#include <sys/ioctl.h>  //These headers are for Cls funciton
#include <unistd.h>

// My own version of cat
char *Type(const char *filename)
{
  int c;
  FILE *f = fopen(filename, "r");
  if(f)
    {
      while((c = getc(f)) != EOF)
	putchar(c);
      fclose(f);
    }
  else
    printf("Can't open file.\n");

  return 0;
}

// Append text to file
char *Append(const char *filename, char *a)
{
  FILE *f = fopen(filename, "a"); // the "a" means append to file
  if(f)
    {
      fprintf(f, a);
      fclose(f);
    }
  else
    printf("Can't append to file.\n");

  return 0;
}

// Copy file
char *Cp(const char *source, const char *target)
{
  char ch;
  FILE *a = fopen(source, "r");
  FILE *b = fopen(target, "w");
  if(a)
    {
      while((ch = fgetc(a)) != EOF)
	{
	  fputc(ch, b);
	}
      fclose(a);
      fclose(b);
    }
  return 0;
}

// Move file
char *Move(const char *source, const char *target)
{
  Cp(source, target);
  remove(source); // Removes orignal file, function in C stdlib

  return 0;
}

// Delete specific line in file
char *Del_line(const char *source, const char *target)
{
  char ch;
  int line, i = 1;
  Type(source);
  printf("Enter line number to delete: \n");
  scanf("%d", &line);
  FILE *a, *b; 
  a = fopen(source, "r");
  b = fopen(target, "w");
  while (ch != EOF)
    {
      ch = getc(a);
      if (ch == '\n')
	{
	  i++;
	  if(i != line)
	    {
	      putc(ch, b);
	    }
	}
		
    }
  // Bug where ascii 152 is being paced at end of file, can't fix :(
  fclose(a);
  fclose(b);
  return 0;
}

//CLears screen, but leaves prompt at bottom lol
char *Cls()
{
  int rows, i;
	
  // Next 3 lines get window height
  struct winsize window;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &window);
  rows = window.ws_row;
	
  // Does the clearing
  for(i = 0; i < rows; i++)
    {
      printf("\n");
    }
  return 0;
}

char *Echo(const char *a)
{
  printf("%s\n", a);

  return 0;
}
