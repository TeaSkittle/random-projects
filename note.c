// Simple note taking app, for todo lists
// Author: Travis Dowd
// Date: 9-20-2019
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tul.h"

// Characters for each note, 256 is good
#define MAX 256   
char buffer[MAX];

/* Location of files, in /tmp
   so will these will not be 
   permanent files */
const char *Source = "/tmp/note.txt";
const char *Target = "/tmp/note_backup.txt";

int main(int argc, char *argv[])
{
    // Help, show arg variables
    if (argc == 2 && (strcmp(argv[1], "help") == 0))
    {
        printf("help     - list commands\n");
        printf("type     - list notes\n");
        printf("new      - add new note\n");
        printf("copy     - make backup of notes\n");
        printf("move     - make backup of notes & delete original\n");
        printf("delete   - delete specific line from notes *slightly buggy*\n");
        printf("remove   - delete all notes\n");
    }
    // Show the current notes
    else if (argc == 2 && (strcmp(argv[1], "type") == 0))
        Type(Source);
    // Add new note
    else if (argc == 2 && (strcmp(argv[1], "new") == 0))
    {
        printf("Enter text: \n");
        fgets(buffer, MAX, stdin);
        Append(Source, buffer);
    }
    // Copy notes
    else if (argc == 2 && (strcmp(argv[1], "copy") == 0))
        Cp(Source, Target);
    // Move notes, not really important here, may delete    
    else if (argc == 2 && (strcmp(argv[1], "move") == 0))
        Move(Source, Target);
    // Delete line from notes
    else if (argc == 2 && (strcmp(argv[1], "del_line") == 0))
        Del_line(Source, Target);
    // Delete notes
    else if (argc == 2 && (strcmp(argv[1], "remove") == 0))
        remove(Source);
    
    return 0;
}