#!/bin/bash
# Simple fetch script
# Date: 9-25-1019

# Based on icyfox's info.sh
user=$(whoami)
kernel=$(uname -r)
shell=$(basename $SHELL)
os=$(source /etc/os-release && echo $ID)
wm=$(xprop -id 0x200000 -notype -len 100 -f _NET_WM_NAME 8t)
wm=${wm/*_NET_WM_NAME = }
wm=${wm/\"}
wm=${wm/\"*}
wm=${wm,,} 
printf "\nTea-Info:\n"
printf "\tShell:\t\t$shell\n"
printf "\tOS:\t\t$os\n"
printf "\tWM:\t\t$wm\n"
printf "\tKernel:\t\t$kernel\n"