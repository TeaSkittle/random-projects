# A simple line editor written for POSIX shells
# Autor: Travis Dowd
# Date: 10-31-2019, 11-6-2019

# gets line count from file, to be used as a variable
line_count(){
    wc -l $1 | awk '{ print $1 }'
}

RUN=1
while [ "$RUN" = 1 ]
do
    echo ">"
    read MODE
    case $MODE in
	h)  # display basic help
	    echo "List of available modes:"
	    echo -e "\t o - output file"
	    echo -e "\t a - append to file"
	    echo -e "\t i - insert at line number"
	    echo -e "\t d - delete specific line number"
	    echo -e "\t h - help"
	    echo -e "\t q - quit"
	    ;;
	o)  # output file
	    cat $1 | nl -v 0 # cat with line #, starting at 0
	    ;;
	a)  # append to file, add new line
	    echo "Text:"
	    read INPUT
	    echo $INPUT >> $1
	    ;;
	q)  # quit ted
	    RUN=0
	    ;;
	d)  # delete line
	    echo "Line Number:"
	    read LINUM
	    if [ "$LINUM" -gt "0" ]; then	
		awk -v n=$LINUM 'NR == n+1 {next} {print}' $1 > .backup
	    elif [ "LINUM" -eq "0" ]; then
		awk -v n=$LINUM 'NR == n {next} {print}' $1 > .backup
	    fi
	    mv .backup $1
	    ;;
	i)  # insert at line number
	    echo "Line Number:"
	    read LINUM
	    # insert at start of file, line 0
	    if [ "$LINUM" -eq "0" ]; then
		echo "Text:"
		read TEXT
		echo $TEXT > .backup
		cat $1 >> .backup
	    # insert elsewhere in file
	    elif [ "$LINUM" -gt "0" ]; then
  		echo "Text:"
		read TEXT
		cp $1 .backup
		# Kind of confusing, but this splits the file
		LINE=$(line_count .backup)
		let LINE2=$LINE-$LINUM
		head -n $LINUM $1 > .backup
		echo $TEXT >> .backup
		tail -n $LINE2 $1 >> .backup
	    fi
	    mv .backup $1
	    ;;
	*)  # any other cmd, error checking
	    echo "ERROR: Unkown command..."
	    ;;
    esac
done

