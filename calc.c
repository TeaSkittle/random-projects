// Polish Notation Calculator
// Author: Travis Dowd
// Date: 9-18-2019
#include <stdio.h>
#include <string.h>

char *Help()
{
    printf("\nThis calculator uses Polish Notation\n");
    printf("Instead of A + B, it goes + A B\n");
    printf("The operator goes at the start, here are operators to use:\n");
    printf("\nBasic: + - * /\n");
    printf("Raise A to B: ^\n");
    printf("A squared + B squared: P\n");
    printf("Area of a triangle (A * B)/2: T\n");
    printf("\n");
	
	return 0;
}

int power(double x, double y)
{
    double i;
    double z = 1;
    
    for(i = 0; i < y; i++)
        z *= x;
    
    printf("%.1lf", z);
	
	return 0;
}

int pythag(double x, double y)
{
    double a, b, c;
    a = x * x;
    b = y * y;
    c = a + b;
            
    printf("%.1lf + %.1lf = %.1lf", a, b, c);
	
	return 0;
}

int area(double x, double y)
{
    double a, b;
    a = x * y;
    b = a / 2;
    
    printf("%.1lf", b);

	return 0;
}

int input()
{
    char op;  // operator
    double a, b; // numbers
    
    printf("\n");
    scanf("%c %lf %lf", &op, &a, &b); // enter: '(' number number op ')', polish
    
    switch(op)
    {
        case '+':
            printf("%.1lf", a + b);
            break;
        case '-':
            printf("%.1lf", a - b);
            break;
        case '*':
            printf("%.1lf", a * b);
            break;
        case '/':
            if (b == 0)
            {
                printf("ERROR: Division by zero!\n");
                break;
            }
            else
            {    
                printf("%.1lf", a / b);
                break;
            }
        case '^':
            power(a, b);
            break;
        case 'P':
            pythag(a, b);
            break;
        case 'T':
            area(a, b);
            break;
        default:
            //printf("ERROR: Operator not found!\n");
            break;
    }
    printf("\n");
	return 0;
}

int main(int argc, char **argv)
{
    // cmdline argument for help
    if (argc == 2 && (strcmp(argv[1], "-help") == 0))
        Help();
    else    
        input();
    return 0;
}