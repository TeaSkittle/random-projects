#!/bin/bash
# Author: Travis Dowd
# Date: 9/9/2019
# This script simply starts the wifi connection, needs to be wet up with
# wifi_setup.sh first

echo "== Run with sudo! =="
wpa_supplicant -B -i wlp2s0 -c /etc/wpa_supplicant/wpa_supplicant-wlp2s0.conf
dhcpcd wlp2s0
printf "\nThis uses the default wireless card, if using another edit this file and run again\n"