// A terminal fighting game
// Date: 12-9-19
// Author: Travis Dowd
#include <stdio.h>

#define EXIT    0
#define PUNCH   1
#define BLOCK   2
#define THROW   3
#define PROMPT  "> "

void Input( void );
void Moves( int action, int enemy );
void EnemyList( void );
int Bar( int health, int gauge );
int LifeCheck( void );

int choice;
int run           = 1;
int life          = 3;
int enemyLife     = 3;
char *playerGauge = "***";
char *enemyGauge  = "***";

int main( int argc, char *argv[] ){
  EnemyList();
  Input();
  return 0;
}

void Input( void ){
  int action; 
  while( run > 0 ){
    printf("\n%s", PROMPT);
    scanf("%d", &action);
    Moves(action, choice);
    LifeCheck();
  }
}

int Bar( int health, int gauge){
  if ( gauge == 1 ){            // player guage
    if ( health == 2 ){
      playerGauge = "**";
    } else if ( health == 1){
      playerGauge = "*";
    } else if ( health == 0){
      playerGauge = " ";
    }
  } else if ( gauge == 2){   // enemy gauge
    if ( health == 2 ){
      enemyGauge = "**";
    } else if ( health == 1){
      enemyGauge = "*";
    } else if ( health == 0){
      enemyGauge = " ";
    }      
  }
  printf("Your-Health: %s       \t\tEnemy-Health:%s\n", playerGauge, enemyGauge);
  printf("=========================================================\n\n");
  return 1;
}

int LifeCheck( void ){
  if ( life == 0){
    printf("You lose!\n\n");
    run = 0;
  } else if ( enemyLife == 0){
    printf("You win!\n\n");
    run = 0;
  } else {
    ;
  } return 1;
}

void EnemyList( void ){  
  printf(" _______  __   __  _______  ___      ___             _______  ___   _______  __   __  _______ \n");
  printf("|       ||  | |  ||       ||   |    |   |           |       ||   | |       ||  | |  ||       |\n");
  printf("|  _____||  |_|  ||    ___||   |    |   |     ____  |    ___||   | |    ___||  |_|  ||_     _|\n");
  printf("| |_____ |       ||   |___ |   |    |   |    |____| |   |___ |   | |   | __ |       |  |   |  \n");
  printf("|_____  ||       ||    ___||   |___ |   |___        |    ___||   | |   ||  ||       |  |   |  \n");
  printf(" _____| ||   _   ||   |___ |       ||       |       |   |    |   | |   |_| ||   _   |  |   |  \n");
  printf("|_______||__| |__||_______||_______||_______|       |___|    |___| |_______||__| |__|  |___|\n\n");
  printf("\nPick an opponent\n");
  printf("\t1. Mike Tythun\n");
  printf("\t2. Mr. Biggs\n");
  printf("\t3. Turtleneck Tim\n\n");
  printf("Choose your enemy\n%s", PROMPT);
  scanf("%d", &choice);
  printf("\n\n");
  printf("1 - Punch\n2 - Block\n3 - Throw\n0 - Quit\n\n\n");
  Bar(3, 1);
}

void Moves( int action, int enemy ){
  switch( action ){
  case PUNCH:
    if ( enemy == 1){
      printf("You both punched and missed!\n\n");
      Bar(life, 1);
    } else if ( enemy == 2){
      printf("You landed a punch!\n\n");
      Bar(--enemyLife, 2); 
    } else if ( enemy == 3){
      printf("Your punched was blocked!\n\n");
      Bar(life, 1);
    } break;
  case BLOCK:
    if ( enemy == 1){ 
      printf("You blocked!\n\n");
      Bar(life, 1);
    } else if ( enemy == 2){
      printf("You have been thrown!\n\n");
      Bar(--life, 1);
    } else if ( enemy == 3){
      printf("You both blocked, so nothing happened...\n\n");
      Bar(life, 1);
    } break;
  case THROW:
    if ( enemy == 1){
      printf("You were punched!\n\n");
      Bar(--life, 1);
    } else if ( enemy == 2){
      printf("You both tried to grabbed, so nothing happened...\n\n");
      Bar(life, 1);
    } else if ( enemy == 3){
      printf("You landed a throw!\n\n");
      Bar(--enemyLife, 2);
    } break;
  case EXIT:
    run = 0;
    printf("Thank you for playing!\n\n");
    break;
  default:
    printf("ERROR: Unkown value!\n\n");
    break;
  }
}
