# Random-Projects 

A collection of the smaller things I have worked on and created.
1. HolyC_Overview.HC - Basics of the language HolyC by the late Terry A. Davis
2. calc.c - A basic Polish notation calculator
3. cm360.py - Calculate mouse speed for FPS games
4. note.c - A notetaking app
5. shellfight.c - A text/turn-based fighting game
6. teainfo.sh - Simple fetch script of basic system info
7. ted.sh - A text editor using shell/awk commands, similar to ed.
8. tul.h - Basic system utilities to be included in C files.
9. up_wifi.sh - Start wifi using wpa-supplicant, meant for use in Void Linux
10. wifi__setup.sh - Setup wifi for wpa-supplicant, meant for Void Linux